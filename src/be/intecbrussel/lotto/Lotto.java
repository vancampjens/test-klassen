package be.intecbrussel.lotto;

import java.util.*;

public class Lotto {

	public static void main(String[] args) {

		System.out.println(Arrays.toString(be.intecbrussel.lotto.Lotto.trekking()));
		System.out.println("-------------------------");
		System.out.println(reserve());

	}

	public static int[] trekking() {

		Random rand = new Random();

		int[] lottery = new int[6];

		for (int i = 0; i < lottery.length; i++) {
			int randomNums = rand.nextInt(45) + 1;
			if (i == randomNums) {
				randomNums = i + 1;
			}
			lottery[i] = randomNums;
		}

		return lottery;

	}

	public static int reserve() {
		int reserve = 0;
		Random randReserve = new Random();

		for (int i = 0; i <= 6; i++) {
			reserve = randReserve.nextInt(45) + 1;
			if (i == reserve) {
				reserve = i + 1;
			}

		}

		return reserve;

	}

}
