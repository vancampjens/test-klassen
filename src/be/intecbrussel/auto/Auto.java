package be.intecbrussel.auto;

public class Auto {

	private String merknaam;
	private String nummerplaat;
	private Motor motor = new Motor();

	public Auto(String merknaam, String nummerplaat) {

		this.merknaam = merknaam;
		this.nummerplaat = nummerplaat;
	}

	public Auto(String merknaam) {

		this.merknaam = merknaam;
	}

	public Auto(Auto kopie) {

		merknaam = kopie.merknaam;
		nummerplaat = kopie.nummerplaat;
		motor = kopie.motor;
	}

	public void geefGas() {

	}

	public void geefGas(int gas) {

		motor.verhoogToerental(motor.getToerental() + gas);

	}

	public void rem() {

		motor.setToerental(0);

	}

	public void toonToerental() {
		System.out.println(motor.getToerental());
	}

	@Override
	public String toString() {
		return "VW Golf met kenteken BHZ456";
	}

}
