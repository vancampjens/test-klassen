package be.intecbrussel.auto;

public class Motor {

	private int MAX_TOERENTAL = 5800;
	private int toerental;

	public Motor() {

	}

	public void verhoogToerental(int verhoogToerental) {

		setToerental(getToerental() + verhoogToerental);
	}

	public void setToerental(int toerental) {

		this.toerental = toerental > MAX_TOERENTAL ? MAX_TOERENTAL : toerental;
	}

	public int getToerental() {

		return toerental;
	}

}
